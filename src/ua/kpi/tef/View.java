package ua.kpi.tef;

/**
 * Created by User on 17.03.2016.
 */
public class View {
    // Text's constants
    public static final String INPUT_INT_DATA = "Input INT value = ";
    public static final String INPUT_INT_DATA_MIN = "Input INT min = ";
    public static final String INPUT_INT_DATA_MAX = "Input INT max = ";
    public static final String WRONG_INPUT_INT_DATA = "Wrong input! Repeat please! ";
    public static final String OUR_INT = "Guessed number = ";
//    public static final String QUESTION = "Is your number '>','<' or '=' this number %d ?";

    public void printMessage(String message){
        System.out.println(message);
    }

    public void printMessageAndInt(String message, int value){
        System.out.println(message + value);
    }

}
